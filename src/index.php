<?php
	class Index {
		public $response;

		function send_response() {
			return $this->response;
		}

		function set_response($response) {
			$this->response = $response;
		}

	}

	$object = new Index();
	$object->set_response('Hello, World!');
	echo $object->send_response();
	$object->set_response('<br><b>DEVELOPMENT ENVIRONMENT</b>');
	echo $object->send_response();
?>
