<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

final class ClassTest extends TestCase
{
	public function testCanBeCreatedFromValidEmailAddress(): void
	{
		$this->assertInstanceOf(
			cls::class,
			cls::fromString('max@austwel.xyz')
		);
	}

	public function testCannotBeCreatedFromInvalidEmailAddress(): void
	{
		$this->expectException(InvalidArgumentException::class);

		cls::fromString('invalid');
	}

	public function testCanBeUsedAsString(): void
	{
		$this->assertEquals(
			'max@austwel.xyz',
			cls::fromString('max@austwel.xyz')
		);
	}
}
